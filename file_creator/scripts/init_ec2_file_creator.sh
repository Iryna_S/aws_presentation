#!/bin/bash

sudo apt update
sudo apt install python3-pip

pip3 install discord.py
pip3 install boto3
pip3 install configparser2

mkdir -p  ~/.aws

echo [default] > ~/.aws/credentials
echo aws_access_key_id = $AWS_ACCESS_KEY  >> ~/.aws/credentials
echo aws_secret_access_key = $AWS_SECRET_ACCESS_KEY >> ~/.aws/credentials

echo [default] > ~/.aws/config
echo region=us-east-1d >> ~/.aws/config

sudo apt-get install postfix

#MAILTO=""
(crontab -l 2>/dev/null; echo "*/5 * * * * /home/ubuntu/scripts/file_creator.py /home/ubuntu/scripts/ >> /home/ubuntu/crontab.log 2>&1") | crontab -
