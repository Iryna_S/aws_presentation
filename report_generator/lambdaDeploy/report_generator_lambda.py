import json
import boto3
import pymysql
import csv
import io

ssm = boto3.client('ssm')

def get_secret(key, withDecryption=False):
	resp = ssm.get_parameter(
		Name=key,
		WithDecryption=withDecryption
	)
	return resp['Parameter']['Value']

rds_host=get_secret('rds_host')
username=get_secret('username')
password=get_secret('password', True)
db_name=get_secret('db_name')

query="""select concat(date_format(t.date_start, '%m-%d-%Y %H'), ':', concat(round(date_format(t.date_start, '%i') / 10)), '0') start_date, 
      t.name_of_show, 
      count(f.viewer_id) as number_of_viewers, 
      concat(round(sum(f.end_watching-f.start_watching) / 3600), ':', lpad(round((sum(f.end_watching-f.start_watching) % 3600) / 60), 2, '0')) as time_of_viewing
      from tvshow t inner join facttable f
      where f.show_id = t.nnumber
      group by 1, 2
      order by 1, 2"""

delete_facttable="""delete from facttable where show_id in 
                 (select nnumber from tvshow where concat(date_format(date_start, '%%m-%%d-%%Y %%H'), ':', concat(round(date_format(date_start, '%%i') / 10)), '0') = "%s")"""
delete_showtv="""delete from tvshow where concat(date_format(date_start, '%%m-%%d-%%Y %%H'), ':', concat(round(date_format(date_start, '%%i') / 10)), '0') = '%s'"""

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('reports')

def lambda_handler(event, context):
    conn = pymysql.connect(rds_host, user=username, passwd=password, db=db_name, connect_timeout=15)

    reports = {}
    with conn.cursor() as cur:
       cur.execute(query)

       for row in cur:
           row_list = list(row)

           if row_list[0] not in reports:
                reports[row_list[0]] = []

           reports[row_list[0]].append(row_list)

       cur.close()

    result = []
    for key in reports:
        csvio = io.StringIO()
        writer = csv.writer(csvio)

        for line in reports[key]:
            writer.writerow(line[1:])

        table.put_item(
            Item={
                'statistics_string': key,
                'report': csvio.getvalue()
            }
        )

        csvio.close()

        try:
            with conn.cursor() as cur:
                cur.execute(delete_facttable % (key))
                cur.close()

            with conn.cursor() as cur:
                cur.execute(delete_showtv % (key))
                cur.close()

            conn.commit()
            conn.close()
        except:
            print('"errorType": "InterfaceError", "errorMessage": "(0, '')" issue with cursors?')

        result.append(key)

    return {
        'statusCode': 200,
        'body': result
    }
