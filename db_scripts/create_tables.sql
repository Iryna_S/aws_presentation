create database db_show;

use db_show;

create table db_show.tvshow (
    nnumber integer,
    name_of_show varchar(60),
    channel varchar(50),
    date_start timestamp,
    date_end timestamp);

create table db_show.facttable(
    viewer_id integer,
    show_id integer,
    start_watching timestamp,
    end_watching timestamp);

create table db_show.house_hold_member(
    id_member integer,
    house_hold_id integer,
    age integer,
    gender varchar(10));

create table db_show.house_hold(
    house_hold_id integer,
    house_hold_size integer,
    address varchar(50));
