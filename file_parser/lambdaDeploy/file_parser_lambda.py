import boto3
import pymysql
from datetime import datetime

s3 = boto3.resource('s3')
ssm = boto3.client('ssm')

def get_secret(key, withDecryption=False):
	resp = ssm.get_parameter(
		Name=key,
		WithDecryption=withDecryption
	)
	return resp['Parameter']['Value']

rds_host=get_secret('rds_host')
username=get_secret('username')
password=get_secret('password', True)
db_name=get_secret('db_name')

def parse_file(tvshow_obj, facttable_obj):
    tvshow_file = tvshow_obj.get()['Body'].read().decode('utf-8')
    facttable_file = facttable_obj.get()['Body'].read().decode('utf-8')

    conn = pymysql.connect(rds_host, user=username, passwd=password, db=db_name, connect_timeout=15)

    tvshow_lines = tvshow_file.split('\r')
    for line in tvshow_lines:
        args = line.strip().split('\t')
        if (len(args) > 1):
            with conn.cursor() as cur:
                start = datetime.strptime(args[3], '%Y.%m.%d.%H.%M.%S')
                end = datetime.strptime(args[4], '%Y.%m.%d.%H.%M.%S')

                cur.execute("""insert into db_show.tvshow(nnumber, name_of_show, channel, date_start, date_end) 
                                        values( %s, "%s", "%s", "%s", "%s" )""" % (args[0], args[1], args[2], start, end))

    facttable_lines = facttable_file.split('\r')
    for line in facttable_lines:
        args = line.strip().split('\t')
        if (len(args) > 1):
            with conn.cursor() as cur:
                start = datetime.strptime(args[2], '%Y.%m.%d.%H.%M.%S')
                end = datetime.strptime(args[3], '%Y.%m.%d.%H.%M.%S')

                cur.execute("""insert into db_show.facttable(viewer_id, show_id, start_watching, end_watching) 
                                        values( %s, %s, "%s", "%s" )""" % (args[0], args[1], start, end))


    conn.commit()
    cur.close()

    tvshow_obj.delete()
    facttable_obj.delete()

#reads s3 bucket and forms pairs of files to process
def lambda_handler(event, context):
    bucket = s3.Bucket('bucket-nvirgin')

    bucket_objects = []
    for obj in bucket.objects.all():
        bucket_objects.append((obj.key, obj))

    def takeFirst(elem):
        return elem[0]
    bucket_objects.sort(key=takeFirst, reverse=True)

    file_list = []
    facttable_dict = {}
    for obj in bucket_objects:
        key = obj[1].key

        key_list = key.split('_')
        if (key_list[0] == 'tvshow'):
            file_list.append((obj[1], 'facttable_' + key_list[1]))
        else:
            facttable_dict[key] = obj[1]

    file_list_res = []
    for files in file_list:
        tvshow_obj, facttable_key = files
        if facttable_key in facttable_dict:
            parse_file(tvshow_obj, facttable_dict[facttable_key])

        file_list_res.append((tvshow_obj.key, facttable_key))

    return {
        'statusCode': 200,
        'body': file_list_res
    }
