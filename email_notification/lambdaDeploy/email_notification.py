import json
import boto3

sns = boto3.client('sns')
ssm = boto3.client('ssm')

def get_secret(key, withDecryption=False):
	resp = ssm.get_parameter(
		Name=key,
		WithDecryption=withDecryption
	)
	return resp['Parameter']['Value']

arn=get_secret('email_topic')

def lambda_handler(event, context):
    message = 'New reports have been loaded to DynamoDB'

    response = sns.publish(
        TargetArn=arn,
        Message=json.dumps({'default': 'DynamoDB notification',
                            'sms': 'DynamoDB notification',
                            'email': json.dumps(message)}),
        Subject='DynamoDB notification: Database was updated.',
        MessageStructure='json'
    )

    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }
