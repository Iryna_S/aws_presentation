#!/usr/bin/env python3

import csv
import random
from datetime import datetime, timedelta, time
import datetime
import errno
import os
import configparser
import shutil
import boto3
import sys

path_to_dir=str(sys.argv[1])

#to get path without file where run the script and use it for create directory and files
def house_hold(house_hold_file):
    with open(house_hold_file, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter='\t')
        for i in range(1,1000):
            items = ['885 West End Ave','280 Undercliff Ave',' 222 W 45th St','100 Christie Heights St','Auf der Heide Park West New York']
            spamwriter.writerow([i,random.randint(1000,5000),random.choice(items)])
        csvfile.close()

def house_hold_members(house_hold_members):
    with open(house_hold_members, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter='\t')
        for i in range(1,1000):
            items_gender = ['F','M']
            spamwriter.writerow([i,random.randint(1,1000),random.randint(20,100),random.choice(items_gender)])
        csvfile.close()

def create_tv_show_file(name_show, channels, tmp_dir, file_name):
    name_s = []
    name_c= []

    with open(name_show, 'r') as name_1:
        for i in name_1:
            name_s.append(i.replace('\n',''))
    name_1.close()
    with open(channels, 'r') as name_2:
        for j in name_2:
            name_c.append(j.replace('\n',''))
    name_2.close()

    myfile = 'tvshow_' + file_name

    with open(os.path.join(tmp_dir, myfile), 'w') as show:
        spamwriter = csv.writer(show, delimiter='\t')
        for i in range(1,1000):
            start = datetime.datetime.now().replace(microsecond=0)
            end = start + timedelta(hours=random.randint(1,4))
            sh = random.choice(name_s)
            ch = random.choice(name_c)
            spamwriter.writerow([i, sh, ch, start.strftime("%Y.%m.%d.%H.%M.%S"), end.strftime("%Y.%m.%d.%H.%M.%S")])
        show.close()
    return myfile

def create_fact_table_file(name_show, house_hold_members, channels, tmp_dir, file_name):
    list_show = []
    list_members = []
    ffile = create_tv_show_file(name_show, channels, tmp_dir, file_name)
    print(os.path.join(tmp_dir, ffile))
    with open(os.path.join(tmp_dir, ffile), 'r') as file:
        for line in file:
            line = line.split('\t')
            lines = []

            start = get_sec(line[3], line)
            end = get_sec(line[4].replace('\n',''), line)

            start_w = float(random.randint(start, end))
            end_w = random.randint(start_w,end)

            lines.append(line[0])
            lines.append(start_w)
            lines.append(end_w)

            list_show.append(lines)

    with open(house_hold_members, 'r') as members_file:
        for members_line in members_file:
            members_line = members_line.split('\t')
            list_members.append(members_line[0])

    file_fact = 'facttable_' + file_name

    with open(os.path.join(tmp_dir, file_fact), 'w') as show_f:
        spamwriter_f = csv.writer(show_f, delimiter='\t')
        for i in range(len(list_show)):
            viewer = random.choice(list_members)
            show_id = list_show[i][0]
            start_show = datetime.datetime.utcfromtimestamp(list_show[i][1])
            end_show = datetime.datetime.utcfromtimestamp(list_show[i][2])
            spamwriter_f.writerow([viewer, show_id, start_show.strftime("%Y.%m.%d.%H.%M.%S"), end_show.strftime("%Y.%m.%d.%H.%M.%S")])
        show_f.close()

def get_sec(date,line):
    try:
        d = datetime.datetime.strptime(date,"%Y.%m.%d.%H.%M.%S")
        epoch = datetime.datetime.utcfromtimestamp(0)
        return  (d - epoch).total_seconds()
    except ValueError:
        print(line)

def move_files_to_output_dir(tmp_dir, output_dir, is_local, s3_bucket_name):

    from os import listdir
    from os.path import isfile, join
    onlyfiles = [f for f in listdir(tmp_dir) if isfile(join(tmp_dir, f))]

    for file in onlyfiles:
        if is_local == 'True':
            shutil.move(os.path.join(tmp_dir, file), os.path.join(output_dir, file))
        else:
            move_files_to_s3(tmp_dir, file, s3_bucket_name)

def move_files_to_s3(tmp_dir, file, s3_bucket_name):
    file_path=os.path.join(tmp_dir, file)
    print('move to s3 ' + file_path)
    s3 = boto3.resource('s3')
    object = s3.Object(s3_bucket_name, file)
    object.put(Body=open(file_path, 'rb'))
    os.remove(file_path)

def main():
    config = configparser.RawConfigParser()
    config.read(path_to_dir + 'FileCreator.properties')

    house_hold(path_to_dir + config.get('FileSection', 'file.house-hold'))

    house_hold_members_=path_to_dir + config.get('FileSection', 'file.house-hold-members')
    house_hold_members(house_hold_members_)

    file_name = datetime.datetime.now().strftime(config.get('FileSection', 'file.date-format'))

    tmp_dir = os.path.join(path_to_dir + config.get('FileSection', 'file.tmp-dir'))
    try:
        os.makedirs(tmp_dir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    channels=path_to_dir + config.get('FileSection', 'file.channels')

    name_show=path_to_dir + config.get('FileSection', 'file.name-show')
    create_tv_show_file(name_show, channels, tmp_dir, file_name)
    create_fact_table_file(name_show, house_hold_members_, channels, tmp_dir, file_name)
    output_dir = os.path.join(config.get('FileSection', 'file.directory'))
    move_files_to_output_dir(tmp_dir, output_dir, config.get('FileSection', 'file.local'),
                             config.get('FileSection', 'file.s3-bucket-name'))

if __name__ == '__main__':
  main()
